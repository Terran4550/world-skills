﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms.Session3
{
    public partial class ReportForm : Form
    {
        public ReportForm()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTable();
        }
        int colmemb = 0;

        void LoadTable()
        {
            colmemb = 0;
            dataGridView1.Rows.Clear();
            foreach (ClassesForSession3.Expert expert in ClassesForSession3.Dictionaryy.Experts)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = expert.id;
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = expert.name;
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = expert.date;
                bool flag = false;

                foreach (ClassesForSession3.Protocol protocol in ClassesForSession3.Dictionaryy.protocols)
                {
                    if (Program.currentCompetition == protocol.idCompetition && comboBox1.SelectedItem.ToString() == protocol.name)
                    {
                        for (int i = 0; i < protocol.experts.Count; i++)
                        {
                            if (protocol.experts[i] == expert.id)
                            {
                                flag = true;
                            }
                        }

                        if (protocol.mem)
                        {
                            for (int i = 0; i < dataGridView1.Rows.Count; i++)
                            {
                                dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;
                            }
                        }
                    }
                }

                if (!flag) dataGridView1.Rows[dataGridView1.Rows.Count - 1].DefaultCellStyle.BackColor = Color.Red;
                else colmemb++;
            }
            label2.Text = "Протокол подписан " + colmemb.ToString() + " из " + 15 + " экспертов";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colmemb > 14)
            {
                MessageBox.Show("Протокол успешно подписан.");
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;
                }
                foreach (ClassesForSession3.Protocol protocol in ClassesForSession3.Dictionaryy.protocols)
                {
                    if (Program.currentCompetition == protocol.idCompetition && comboBox1.SelectedItem.ToString() == protocol.name)
                    {
                        protocol.mem = true;
                    }
                }
            }
            else MessageBox.Show("Подпись невозможна.");
        }

        private void ReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Session2.ProtocolsList f = new Session2.ProtocolsList();
            f.Show();
        }
    }
}
