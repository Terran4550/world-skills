﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms.Session3
{
    public partial class ProtocolRoles : Form
    {
        public ProtocolRoles()
        {
            InitializeComponent();
            LoadTable();
            a = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (ClassesForSession3.Protocol protocol in ClassesForSession3.Dictionaryy.protocols)
            {
                if (Program.currentCompetition == protocol.idCompetition && "Распределение судейских ролей" == protocol.name)
                {
                    bool flag = false;
                    foreach (int exp in protocol.experts)
                    {
                        if (exp == Program.idExpert)
                        {
                            flag = true;
                        }
                    }
                    if (!flag)
                    {
                        protocol.mem = true;
                        MessageBox.Show("Протокол успешно подписан.");
                        dataGridView1.ReadOnly = true;
                        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                        {
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Вы уже подписали текущий протокол.");
                    }
                }
            }

            dataGridView1.Rows.Clear();
            LoadTable();
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!a) return;
            if (e.ColumnIndex == 3 || e.ColumnIndex == 4 || e.ColumnIndex == 5 || e.ColumnIndex == 6 || e.ColumnIndex == 7)
            {
                foreach (ClassesForSession3.Expert expert in ClassesForSession3.Dictionaryy.Experts)
                {
                    if (expert.id.ToString() == dataGridView1[0, e.RowIndex].Value.ToString())
                    {
                        if ((bool)dataGridView1[e.ColumnIndex, e.RowIndex].Value == true)
                        {
                            expert.roles.Add(e.ColumnIndex - 2);
                        }
                        else
                        {
                            expert.roles.Remove(e.ColumnIndex - 2);

                        }
                    }
                }
            }
        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView1.IsCurrentCellDirty)
                dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        bool a = false;

        void LoadTable()
        {
            foreach (ClassesForSession3.Protocol protocol in ClassesForSession3.Dictionaryy.protocols)
            {
                if (Program.currentCompetition == protocol.idCompetition && protocol.name == "Распределение судейских ролей")
                {
                    foreach (ClassesForSession3.Expert expert in ClassesForSession3.Dictionaryy.Experts)
                    {
                        dataGridView1.Rows.Add();
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = expert.id;
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = expert.name;
                        dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = expert.member;
                        for (int j = 0; j < expert.roles.Count; j++)
                        {
                            dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[expert.roles[j] + 2].Value = true;
                        }
                    }

                    label4.Text = "C" + protocol.date.ToString();
                    if (protocol.mem)
                    {
                        dataGridView1.ReadOnly = true;
                        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                        {
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;
                        }
                    }
                }
            }
        }
    }
}
