﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.Windows.Forms;

namespace WorldSkillsAppForms
{
    public class ClassesForSession3
    {
        public class Expert
        {
            public int id { set; get; }
            public string name { set; get; }
            public string date { set; get; }
            public string member { set; get; }
            public List<int> roles { set; get; }

            public Expert(int id, string name, string date, string member, List<int> role)
            {
                this.id = id;
                this.name = name;
                this.date = date;
                this.member = member;
                this.roles = role;
            }
        }

        public class Protocol
        {
            public int idCompetition { set; get; }
            public string name { set; get; }
            public int date { set; get; }
            public List<int> experts { set; get; }
            public bool mem { set; get; }

            public Protocol(int idCompetition, string name, int date, List<int> experts, bool mem)
            {
                this.idCompetition = idCompetition;
                this.name = name;
                this.date = date;
                this.experts = experts;
                this.mem = mem;
            }
        }


        public static class Dictionaryy
        {
            public static List<Protocol> protocols = new List<Protocol>
            {
                new Protocol(9, "Регистрация экспертов на площадке", -2, new List<int>{1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15}, false),
                new Protocol(9, "Ознакомление экспертов с техникой безопасности", -2, new List<int>{1, 2,15}, false),
                new Protocol(9, "Внесение 30% изменений в задание", -1, new List<int>{1, 2, 3, 4, 5, 13, 14, 15}, false),
                new Protocol(9, "Ознакомление с ведомостями оценки", -1, new List<int>{1, 2, 4, 5, 8, 9, 10, 11, 13, 14}, false),
                new Protocol(9, "Распределение судейских ролей", 1, new List<int>{3, 4}, false),
                new Protocol(17, "Регистрация экспертов на площадке", -2, new List<int>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, false),
                new Protocol(17, "Ознакомление экспертов с техникой безопасности", -1, new List<int>{1, 2, 3, 4, 5}, false),
                new Protocol(17, "Внесение 30% изменений в задание", 1, new List<int>{1, 2, 3, 6, 7, 8, 9, 10}, false),
                new Protocol(17, "Ознакомление с ведомостями оценки", 2, new List<int>{1, 2, 4, 5}, false),
                new Protocol(17, "Распределение судейских ролей", 3, new List<int>{7, 8, 10, 11, 12}, false),
            };

            public static List<Expert> Experts = new List<Expert> {
                new Expert(1, "Сиясинов Мечислав Елисеевич", "29.10.1989", "Друганин Агафон Артемиевич", new List<int>{1}),
                new Expert(2, "Грибкова Элеонора Романовна", "12.03.1973", "Зубарев Давид Серафимович", new List<int>{2}),
                new Expert(3, "Кожухова Фаина Богдановна", "07.03.1994", "Привалов Никанор Матвеевич", new List<int>{3 }),
                new Expert(4, "Ядренников Пимен Ефремович", "26.08.1973", "Фанин Карп Сократович", new List<int>{4}),
                new Expert(5, "Лясковец Рюрик Платонович", "11.11.1978", "Чепурин Валерьян Зиновиевич", new List<int>{5 }),
                new Expert(6, "Ломовцева Пелагея Леонидовна", "03.08.1977", "Вольваков Денис Еремеевич", new List<int>{3, }),
                new Expert(7, "Гурковский Лаврентий Мечиславович", "26.08.1973", "Капица Феоктист Наумович", new List<int>{}),
                new Expert(8, "Набадчиков Севастьян Глебович", "27.03.1989", "Януть Антон Владиславович", new List<int>{2 }),
                new Expert(9, "Бильбасова Элеонора Ипполитовна", "30.06.1990", "Кризько Аристарх Якубович", new List<int>{4 }),
                new Expert(10, "Яркин Всеволод Адамович", "31.08.1999", "Мальцов Валентин Остапович", new List<int>{ 5 }),
                new Expert(11, "Глушаков Владлен Владиславович", "30.06.1970", "Трифонов Онуфрий Валерьянович", new List<int>{}),
                new Expert(12, "Гришко Петр Данилевич", "02.05.1965", "Меншиков Иннокентий Герасимович", new List<int>{ }),
                new Expert(13, "Первак Родион Феликсович", "03.06.1976", "Ямзин Валерьян Ипатович", new List<int>{2 }),
                new Expert(14, "Сталин Филимон Никонович", "29.11.1999", "Лысов Антон Фролович", new List<int>{5 }),
                new Expert(15, "Ивнаов Аркадий Анатольевич", "09.11.1956", "Филипов Архип Иванович", new List<int>{4}),
            };


        }
    }
}
