﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.OleDb;

namespace WorldSkillsAppForms.Session3
{
    public partial class MainExpertsList : Form
    {
        System.Data.DataTable data;
        public MainExpertsList()
        {
            InitializeComponent();


            String name = "Лист1";
            String filenamewithpath = @"C:\\Users\\Алина\\Documents\\users.xls";

            String constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filenamewithpath +
                                     ";Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\"";

            OleDbConnection con = new OleDbConnection(constr);
            OleDbCommand oconn = new OleDbCommand("Select * From [" + name + "$]", con);
            con.Open();

            OleDbDataAdapter sda = new OleDbDataAdapter(oconn);
            data = new System.Data.DataTable();
            sda.Fill(data);
            dataGridView1.DataSource = data;
            con.Close();


            List<string> columnArray = new List<string>();
            DataTable dt = (DataTable)dataGridView1.DataSource;

            for (int q = 2; q < 4; q++)
                comboBox1.Items.Add(dt.Rows[0][q].ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DetailReport form = new DetailReport();

            // Show the settings form
            form.Show();
        }

        private void copyAlltoClipboard()
        {
            //to remove the first blank column from datagridview
            // dataGridView1.RowHeadersVisible = false;
            dataGridView1.SelectAll();
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occurred while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.DefaultView.RowFilter = string.Format("[{0}] LIKE '%{1}%'", "F4", comboBox1.SelectedItem);
            dataGridView1.DataSource = dt;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.DefaultView.RowFilter = string.Format("[{0}] LIKE '%{1}%'", "F3", comboBox2.SelectedItem);
            dataGridView1.DataSource = dt;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.DefaultView.RowFilter = string.Format("[{0}] LIKE '%{1}%'", "F3", comboBox2.SelectedItem);
            dataGridView1.DataSource = dt;
        }
    }
}
