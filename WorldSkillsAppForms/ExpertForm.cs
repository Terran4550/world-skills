﻿using System;
using System.Data.OleDb;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorldSkillsAppForms.Session2;

namespace WorldSkillsAppForms
{
    public partial class ExpertForm : Form
    {
        System.Data.DataTable data;
        public ExpertForm()
        {
            InitializeComponent();
            if (Program.currentCompetition == 9) label2.Text += "Программные решения для бизнеса";
            if (Program.currentCompetition == 17) label2.Text += "Веб-дизайн";
            if (Program.currentCompetition == 39) label2.Text += "Сетевое и системное администрирование";
            textBox1.Text = Program.codeExpert;

            foreach (ClassesForSession3.Expert expert in ClassesForSession3.Dictionaryy.Experts)
            {
                if (Program.idExpert == expert.id)
                {
                    label3.Text += expert.name;
                }
            }

            String name = "Лист1";
            String filenamewithpath = @"C:\\Users\\Алина\\Documents\\Organizer.xls";

            String constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filenamewithpath +
                                     ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";

            OleDbConnection con = new OleDbConnection(constr);
            OleDbCommand oconn = new OleDbCommand("Select * From [" + name + "$]", con);
            con.Open();

            OleDbDataAdapter sda = new OleDbDataAdapter(oconn);
            data = new System.Data.DataTable();
            sda.Fill(data);
            dataGridView1.DataSource = data;
            con.Close();


            List<string> columnArray = new List<string>();
            DataTable dt = (DataTable)dataGridView1.DataSource;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PaticipantsListForm form = new PaticipantsListForm();         
            // Show the settings form
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog1.FileName;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ExpertsListForm form = new ExpertsListForm();

            // Show the settings form
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ProtocolsList form = new ProtocolsList();

            // Show the settings form
            form.Show();
        }
    }
}
