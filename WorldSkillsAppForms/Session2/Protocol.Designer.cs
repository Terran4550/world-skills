﻿namespace WorldSkillsAppForms.Session2
{
    partial class Protocol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox_shtamp = new System.Windows.Forms.TextBox();
            this.textBox_instruct = new System.Windows.Forms.TextBox();
            this.label_shtam = new System.Windows.Forms.Label();
            this.label_instruct = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Column1,
            this.Column2});
            this.dataGridView1.Location = new System.Drawing.Point(21, 11);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(625, 237);
            this.dataGridView1.TabIndex = 10;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Id";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ФИО";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 400;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Дата рождения";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(152, 265);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(73, 20);
            this.textBox3.TabIndex = 18;
            this.textBox3.Text = "16.05.2019";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(230, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "С";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(458, 265);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(188, 49);
            this.button1.TabIndex = 16;
            this.button1.Text = "Использовать электронное подписание";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // textBox_shtamp
            // 
            this.textBox_shtamp.Location = new System.Drawing.Point(152, 295);
            this.textBox_shtamp.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_shtamp.Name = "textBox_shtamp";
            this.textBox_shtamp.ReadOnly = true;
            this.textBox_shtamp.Size = new System.Drawing.Size(73, 20);
            this.textBox_shtamp.TabIndex = 15;
            this.textBox_shtamp.Text = "CD6SG23";
            this.textBox_shtamp.Visible = false;
            // 
            // textBox_instruct
            // 
            this.textBox_instruct.Location = new System.Drawing.Point(152, 295);
            this.textBox_instruct.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_instruct.Name = "textBox_instruct";
            this.textBox_instruct.ReadOnly = true;
            this.textBox_instruct.Size = new System.Drawing.Size(163, 20);
            this.textBox_instruct.TabIndex = 14;
            this.textBox_instruct.Text = "Королев Игорь Васильевич";
            this.textBox_instruct.Visible = false;
            // 
            // label_shtam
            // 
            this.label_shtam.AutoSize = true;
            this.label_shtam.Location = new System.Drawing.Point(41, 298);
            this.label_shtam.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_shtam.Name = "label_shtam";
            this.label_shtam.Size = new System.Drawing.Size(100, 13);
            this.label_shtam.TabIndex = 13;
            this.label_shtam.Text = "Временной штамп";
            this.label_shtam.Visible = false;
            // 
            // label_instruct
            // 
            this.label_instruct.AutoSize = true;
            this.label_instruct.Location = new System.Drawing.Point(34, 295);
            this.label_instruct.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_instruct.Name = "label_instruct";
            this.label_instruct.Size = new System.Drawing.Size(107, 13);
            this.label_instruct.TabIndex = 12;
            this.label_instruct.Text = "Инструктаж провёл";
            this.label_instruct.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 268);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Дата протокола";
            // 
            // Protocol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 332);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_shtamp);
            this.Controls.Add(this.textBox_instruct);
            this.Controls.Add(this.label_shtam);
            this.Controls.Add(this.label_instruct);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Protocol";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Протокол: ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Protocol_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox_shtamp;
        private System.Windows.Forms.TextBox textBox_instruct;
        private System.Windows.Forms.Label label_shtam;
        private System.Windows.Forms.Label label_instruct;
        private System.Windows.Forms.Label label1;
    }
}