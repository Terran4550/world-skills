﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms.Session2
{
    public partial class ProtocolsList : Form
    {
        public ProtocolsList()
        {
            InitializeComponent();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            // ВСЕ ПРАВЫЕ БОКОВЫЕ КНОПКИ (5ШТ ЭТО ТОЖЕ ЭКРАН ПРОТОКОЛ - СОЗДАЙТЕ РАЗНЫЕ ИНИЦИАЛИЗАТОРЫ И ВСЁ)
            // Если открытие для эксперта то используйте ExpertForm вместо OrganizatorForm
            Protocol form = new Protocol();

            // Show the settings form
            form.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Protocol f = new Protocol("Регистрация экспертов на площадке");
            f.Show();
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Protocol f = new Protocol("Ознакомление экспертов с техникой безопасности");
            f.Show();
            this.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Protocol f = new Protocol("Внесение 30% изменений в задание");
            f.Show();
            this.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Protocol f = new Protocol("Ознакомление с ведомостями оценки");
            f.Show();
            this.Close();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Protocol f = new Protocol("Распределение судейских ролей");
            f.Show();
            this.Close();
        }

        private void button11_Click_1(object sender, EventArgs e)
        {
            Session3.ReportForm form = new Session3.ReportForm();

            form.Show();
            this.Close();
        }
    }
}
