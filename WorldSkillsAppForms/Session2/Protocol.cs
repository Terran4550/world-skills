﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorldSkillsAppForms.Session3;

namespace WorldSkillsAppForms.Session2
{
    public partial class Protocol : Form
    {
        public Protocol()
        {
            InitializeComponent();
        }
        string nameProt;
        public Protocol(string nameProt)
        {
            this.nameProt = nameProt;
            InitializeComponent();
            this.Text += nameProt;
            LoadTable();

            if (nameProt == "Ознакомление экспертов с техникой безопасности")
            {
                label_instruct.Visible = true;
                textBox_instruct.Visible = true;
            }

            if (nameProt == "Ознакомление с ведомостями оценки")
            {
                label_instruct.Visible = true;
                textBox_instruct.Visible = true;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ReportForm form = new ReportForm();

            // Show the settings form
            form.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            foreach (ClassesForSession3.Protocol protocol in ClassesForSession3.Dictionaryy.protocols)
            {
                if (Program.currentCompetition == protocol.idCompetition && nameProt == protocol.name)
                {
                    bool flag = false;
                    foreach (int exp in protocol.experts)
                    {
                        if (exp == Program.idExpert)
                        {
                            flag = true;
                        }
                    }
                    if (!flag)
                    {
                        protocol.experts.Add(Program.idExpert);
                        MessageBox.Show("Протокол успешно подписан.");
                    }
                    else
                    {
                        MessageBox.Show("Вы уже подписали текущий протокол.");
                    }

                }
            }

            dataGridView1.Rows.Clear();
            LoadTable();
        }

        void LoadTable()
        {
            foreach (ClassesForSession3.Protocol protocol in ClassesForSession3.Dictionaryy.protocols)
            {
                if (Program.currentCompetition == protocol.idCompetition && nameProt == protocol.name)
                {
                    for (int i = 0; i < protocol.experts.Count; i++)
                    {
                        foreach (ClassesForSession3.Expert expert in ClassesForSession3.Dictionaryy.Experts)
                        {
                            if (protocol.experts[i] == expert.id)
                            {
                                dataGridView1.Rows.Add();
                                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = expert.id;
                                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = expert.name;
                                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[2].Value = expert.date;
                            }
                        }
                    }
                    label4.Text = "C" + protocol.date.ToString();
                    if (protocol.mem)
                    {
                        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                        {
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;
                        }
                    }
                }
            }
        }

        private void Protocol_FormClosing(object sender, FormClosingEventArgs e)
        {
            ProtocolsList p = new ProtocolsList();
            p.Show();
        }
    }
}
