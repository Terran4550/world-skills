﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WorldSkillsAppForms.Session2;
using WorldSkillsAppForms.Session3;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.OleDb;

namespace WorldSkillsAppForms
{
    public partial class OrganizatorForm : Form
    {
        public OrganizatorForm()
        {
            InitializeComponent();
            String name = "Лист1";
            String filenamewithpath = @"C:\\Users\\Алина\\Documents\\Organizer.xls";

            String constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filenamewithpath +
                                     ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";

            OleDbConnection con = new OleDbConnection(constr);
            OleDbCommand oconn = new OleDbCommand("Select * From [" + name + "$]", con);
            con.Open();

            OleDbDataAdapter sda = new OleDbDataAdapter(oconn);
            data = new System.Data.DataTable();
            sda.Fill(data);
            dataGridView1.DataSource = data;
            con.Close();


            List<string> columnArray = new List<string>();
            DataTable dt = (DataTable)dataGridView1.DataSource;
            
        }
        System.Data.DataTable data;

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PaticipantsListForm form = new PaticipantsListForm();

            // Show the settings form
            form.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MainSettings form = new MainSettings();

            // Show the settings form
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MainExpertsList form = new MainExpertsList();

            // Show the settings form
            form.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ProtocolReport form = new ProtocolReport();

            // Show the settings form
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog1.FileName;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ProtocolRoles f = new ProtocolRoles();
            f.Show();
        }
    }
}
