﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms
{
    static class Program
    {
        public static string[] codes = new string[] { "TSU4FG5", "YS7DBK4", "JDHBF1HJ" };
        public static string codeExpert = "TSU1641";
        public static int idExpert = 6;
        public static int currentCompetition = 9;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
