﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            StreamReader srRemember = new StreamReader("RememberMe.txt");
            string line = "";
            line = srRemember.ReadLine();
            srRemember.Close();                

            InitializeComponent();
            if (line != "" && line != null)
            {
                textBox1.Text = line.Split(' ')[0];
                textBox2.Text = line.Split(' ')[1];
                switch (textBox1.Text)
                {
                    case "expert":
                        ExpertForm form1 = new ExpertForm();
                        form1.Show();
                        break;
                    case "organizer":
                        OrganizatorForm form2 = new OrganizatorForm();
                        form2.Show();
                        break;

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StreamReader srLogPass = new StreamReader("LoginPassword.txt");
            Dictionary<string, string> LogPass = new Dictionary<string, string>();
            string line = "";
            while ((line = srLogPass.ReadLine()) != null)
            {
                LogPass.Add(line.Split(' ')[0], line.Split(' ')[1]);
            }

            bool LogPassCorrect = false;
            foreach(KeyValuePair<string, string> pair in LogPass)
            {
                if(pair.Key == textBox1.Text && pair.Value == textBox2.Text)
                {
                    LogPassCorrect = true;
                }
            }            

            if (LogPassCorrect)
            {
                StreamWriter swRemember = new StreamWriter("RememberMe.txt");
                if (checkBox1.Checked)
                {
                    swRemember.WriteLine(textBox1.Text + " " + textBox2.Text);
                }
                else
                {
                    swRemember.WriteLine("");
                }
                swRemember.Close();

                switch(textBox1.Text)
                {
                    case "expert":
                        ExpertForm form1 = new ExpertForm();
                        form1.Show();
                        break;
                    case "organizer":
                        OrganizatorForm form2 = new OrganizatorForm();
                        form2.Show();
                        break;

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Session4.AutorizationOnCode f = new Session4.AutorizationOnCode();
            f.Show();
            this.Visible = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
