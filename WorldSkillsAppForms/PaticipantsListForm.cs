﻿using System;
using System.Data.OleDb;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.CSharp.RuntimeBinder;

namespace WorldSkillsAppForms
{
    public partial class PaticipantsListForm : Form
    {
        System.Data.DataTable data;
        public PaticipantsListForm()
        {
            InitializeComponent();


            String name = "Лист1";
            String filenamewithpath = @"C:\\Users\\Алина\\Documents\\Participants.xls";

            String constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filenamewithpath +
                                     ";Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\"";

            OleDbConnection con = new OleDbConnection(constr);
            OleDbCommand oconn = new OleDbCommand("Select * From [" + name + "$]", con);
            con.Open();

            OleDbDataAdapter sda = new OleDbDataAdapter(oconn);
            data = new System.Data.DataTable();
            sda.Fill(data);
            dataGridView1.DataSource = data;
            con.Close();


            List<string> columnArray = new List<string>();
            DataTable dt = (DataTable)dataGridView1.DataSource;

            for (int q = 0; q < dt.Columns.Count; q++)
                comboBox1.Items.Add(dt.Rows[0][q].ToString());

            //comboBox2.SelectedItem = comboBox1.Items[0];
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.DefaultView.RowFilter = string.Format("[{0}] LIKE '%{1}%'", string.Concat("F", (comboBox1.SelectedIndex + 1).ToString()), comboBox2.SelectedItem);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                DataTable dt = (DataTable)dataGridView1.DataSource;
                dt.DefaultView.RowFilter = string.Format("[{0}] LIKE '%{1}%'", "F7", "Не подтвержден");
                dataGridView1.DataSource = dt;
            }
            else
            {
                String name = "Лист1";
                String filenamewithpath = @"C:\\Users\\Алина\\Documents\\Participants.xls";

                String constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filenamewithpath +
                                         ";Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\"";

                OleDbConnection con = new OleDbConnection(constr);
                OleDbCommand oconn = new OleDbCommand("Select * From [" + name + "$]", con);
                con.Open();

                OleDbDataAdapter sda = new OleDbDataAdapter(oconn);
                data = new System.Data.DataTable();
                sda.Fill(data);
                dataGridView1.DataSource = data;
                con.Close();
            }
        }
        private void copyAlltoClipboard()
        {
            //to remove the first blank column from datagridview
            // dataGridView1.RowHeadersVisible = false;
            dataGridView1.SelectAll();
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occurred while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //SaveFileDialog sfd = new SaveFileDialog();
            //sfd.Filter = "Excel Documents (*.xls)|*.xls";
            //sfd.FileName = "Participants.xls";
            //if (sfd.ShowDialog() == DialogResult.OK)
            {
                // Copy DataGridView results to clipboard

                copyAlltoClipboard();

                object misValue = System.Reflection.Missing.Value;
                Excel.Application xlexcel = new Excel.Application();

                xlexcel.DisplayAlerts = false; // Without this you will get two confirm overwrite prompts
                Excel.Workbook xlWorkBook = xlexcel.Workbooks.Add(misValue);
                Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                // Format column D as text before pasting results, this was required for my data
                //Excel.Range rng = xlWorkSheet.get_Range("D:D").Cells;
                //rng.NumberFormat = "@";

                // Paste clipboard results to worksheet range
                Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[1, 1];
                CR.Select();
                xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);

                // For some reason column A is always blank in the worksheet. ¯\_(ツ)_/¯
                // Delete blank column A and select cell A1
                //Excel.Range delRng = xlWorkSheet.get_Range("A:A").Cells;
                //delRng.Delete(Type.Missing);
                //xlWorkSheet.get_Range("A1").Select();

                // Save the excel file under the captured location from the SaveFileDialog
                xlWorkBook.SaveAs("Participants.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlexcel.DisplayAlerts = true;
                xlWorkBook.Close(true, misValue, misValue);
                xlexcel.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlexcel);

                // Clear Clipboard and DataGridView selection
                Clipboard.Clear();
                dataGridView1.ClearSelection();
                // dataGridView1.RowHeadersVisible = true;

                // Open the newly saved excel file
                //if (File.Exists(sfd.FileName))
                //    System.Diagnostics.Process.Start(sfd.FileName);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> columnArray = new List<string>();
            DataTable dt = (DataTable)dataGridView1.DataSource;

            switch (comboBox1.SelectedItem)
            {
                case "№":
                    comboBox2.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBox2.Items.Add(row[0].ToString());
                    }
                    comboBox2.Items.RemoveAt(0);
                    comboBox2.SelectedItem = comboBox1.Items[0];
                    break;
                case "Фамилия":
                    comboBox2.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBox2.Items.Add(row[1].ToString());
                    }
                    comboBox2.Items.RemoveAt(0);
                    comboBox2.SelectedItem = comboBox1.Items[0];
                    break;
                case "Имя":
                    comboBox2.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBox2.Items.Add(row[2].ToString());
                    }
                    comboBox2.Items.RemoveAt(0);
                    comboBox2.SelectedItem = comboBox1.Items[0];
                    break;
                case "Отчество":
                    comboBox2.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBox2.Items.Add(row[3].ToString());
                    }
                    comboBox2.Items.RemoveAt(0);
                    comboBox2.SelectedItem = comboBox1.Items[0];
                    break;
                case "Дата рождения":
                    comboBox2.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBox2.Items.Add(row[4].ToString());
                    }
                    comboBox2.Items.RemoveAt(0);
                    comboBox2.SelectedItem = comboBox1.Items[0];
                    break;
                case "Полных лет":
                    comboBox2.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBox2.Items.Add(row[5].ToString());
                    }
                    comboBox2.Items.RemoveAt(0);
                    comboBox2.SelectedItem = comboBox1.Items[0];
                    break;
                case "Статус подтверждения":
                    comboBox2.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        comboBox2.Items.Add(row[6].ToString());
                    }
                    comboBox2.Items.RemoveAt(0);
                    comboBox2.SelectedItem = comboBox1.Items[0];
                    break;
            }

        }
    }
}
