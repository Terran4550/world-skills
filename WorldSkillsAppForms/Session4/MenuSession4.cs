﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms.Session4
{
    public partial class MenuSession4 : Form
    {
        public MenuSession4()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormProtocolsSession4 f = new FormProtocolsSession4();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PaticipantsListForm form = new PaticipantsListForm();
            form.Show();
        }
    }
}
