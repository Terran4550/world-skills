﻿namespace WorldSkillsAppForms.Session4
{
    partial class ProtocolSession4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_expert = new System.Windows.Forms.Label();
            this.label_GE = new System.Windows.Forms.Label();
            this.label_nameprotocol = new System.Windows.Forms.Label();
            this.dataGridProtocol = new System.Windows.Forms.DataGridView();
            this.col_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_fio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_place = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_dateofbirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_pin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label_comment = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProtocol)).BeginInit();
            this.SuspendLayout();
            // 
            // label_expert
            // 
            this.label_expert.Location = new System.Drawing.Point(13, 61);
            this.label_expert.Name = "label_expert";
            this.label_expert.Size = new System.Drawing.Size(261, 23);
            this.label_expert.TabIndex = 17;
            this.label_expert.Text = "Эксперт, ответсвенный за проверку документов: ";
            this.label_expert.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_GE
            // 
            this.label_GE.AutoSize = true;
            this.label_GE.Location = new System.Drawing.Point(179, 44);
            this.label_GE.Name = "label_GE";
            this.label_GE.Size = new System.Drawing.Size(95, 13);
            this.label_GE.TabIndex = 16;
            this.label_GE.Text = "ГЭ на площадке: ";
            // 
            // label_nameprotocol
            // 
            this.label_nameprotocol.Location = new System.Drawing.Point(128, 10);
            this.label_nameprotocol.Name = "label_nameprotocol";
            this.label_nameprotocol.Size = new System.Drawing.Size(612, 47);
            this.label_nameprotocol.TabIndex = 15;
            this.label_nameprotocol.Text = "Протокол чемпионата по стандартам WorldSkilld Russia ";
            this.label_nameprotocol.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dataGridProtocol
            // 
            this.dataGridProtocol.AllowUserToAddRows = false;
            this.dataGridProtocol.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridProtocol.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_num,
            this.col_fio,
            this.col_place,
            this.col_dateofbirth,
            this.col_comment,
            this.col_pin});
            this.dataGridProtocol.Location = new System.Drawing.Point(12, 133);
            this.dataGridProtocol.Name = "dataGridProtocol";
            this.dataGridProtocol.RowHeadersVisible = false;
            this.dataGridProtocol.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridProtocol.Size = new System.Drawing.Size(860, 317);
            this.dataGridProtocol.TabIndex = 14;
            this.dataGridProtocol.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridProtocol_CellEndEdit);
            // 
            // col_num
            // 
            this.col_num.HeaderText = "№";
            this.col_num.Name = "col_num";
            this.col_num.ReadOnly = true;
            this.col_num.Width = 50;
            // 
            // col_fio
            // 
            this.col_fio.HeaderText = "ФИО";
            this.col_fio.Name = "col_fio";
            this.col_fio.ReadOnly = true;
            this.col_fio.Width = 300;
            // 
            // col_place
            // 
            this.col_place.HeaderText = "Место";
            this.col_place.Name = "col_place";
            this.col_place.ReadOnly = true;
            this.col_place.Visible = false;
            // 
            // col_dateofbirth
            // 
            this.col_dateofbirth.HeaderText = "Дата рождения";
            this.col_dateofbirth.Name = "col_dateofbirth";
            this.col_dateofbirth.ReadOnly = true;
            this.col_dateofbirth.Visible = false;
            // 
            // col_comment
            // 
            this.col_comment.HeaderText = "Комментарий";
            this.col_comment.Name = "col_comment";
            this.col_comment.Visible = false;
            this.col_comment.Width = 300;
            // 
            // col_pin
            // 
            this.col_pin.HeaderText = "PIN";
            this.col_pin.Name = "col_pin";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(280, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(153, 20);
            this.textBox1.TabIndex = 19;
            this.textBox1.Visible = false;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label_comment
            // 
            this.label_comment.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_comment.Location = new System.Drawing.Point(13, 81);
            this.label_comment.Name = "label_comment";
            this.label_comment.Size = new System.Drawing.Size(850, 49);
            this.label_comment.TabIndex = 18;
            // 
            // ProtocolSession4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.label_expert);
            this.Controls.Add(this.label_GE);
            this.Controls.Add(this.label_nameprotocol);
            this.Controls.Add(this.dataGridProtocol);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label_comment);
            this.Name = "ProtocolSession4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Протокол ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProtocolSession4_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProtocol)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_expert;
        private System.Windows.Forms.Label label_GE;
        private System.Windows.Forms.Label label_nameprotocol;
        private System.Windows.Forms.DataGridView dataGridProtocol;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_fio;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_place;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_dateofbirth;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_comment;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_pin;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label_comment;
    }
}