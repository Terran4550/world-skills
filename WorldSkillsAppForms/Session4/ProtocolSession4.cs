﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms.Session4
{
    public partial class ProtocolSession4 : Form
    {
        ClassesForSession4.Protocol thisprotocol;

        public ProtocolSession4()
        {
            InitializeComponent();
        }

        public ProtocolSession4(ClassesForSession4.Protocol thisprotocol)
        {
            InitializeComponent();
            this.thisprotocol = thisprotocol;
            label_GE.Text += thisprotocol.ge;
            label_expert.Text += thisprotocol.expert;

            if (thisprotocol.id == 1)
            {
                this.Text += " возрастного ценза и регистрации";
                label_nameprotocol.Text += "о регистрации конкурсантов и их соответсвии возратному цензу";
                dataGridProtocol.Columns["col_dateofbirth"].Visible = true;
            }
            if (thisprotocol.id == 2)
            {
                this.Text += " техники безопасности";
                label_nameprotocol.Text += "об ознакомлении участников с правилами техники безопасности и охраны труда";
                label_expert.Text = "Провел инструктаж по ТБ и ТО: ";
                textBox1.Visible = true;
                if (thisprotocol.expert != "") textBox1.Text = thisprotocol.expert;
                dataGridProtocol.Columns["col_comment"].Visible = true;
            }
            if (thisprotocol.id == 3)
            {
                this.Text += " жеребьевки";
                label_nameprotocol.Text += "о жеребьевке по распределению конкурсных мест";
                dataGridProtocol.Columns["col_place"].Visible = true;
                dataGridProtocol.Columns["col_comment"].Visible = true;
                label_comment.Text = "Мы, нижеподписавшиеся Конкурсанты, подтверждаем, что жеребьевка была проведена справедливо и честно. Претензий не имеем.";
            }
            if (thisprotocol.id == 4)
            {
                this.Text += " ознакомления с документацией и рабочими местами";
                label_nameprotocol.Text += "об ознакомлении конкурсантов с конкурсной документацией, оборудованием и рабочими местами";
                dataGridProtocol.Columns["col_comment"].Visible = true;
                label_comment.Text = "Мы, нижеподписавшиеся Конкурсанты, подтверждаем, что нам была предоставлена возможность " +
                    "полноценно ознакомиться с актуальными конкурсным заданием, критериями оценки, регламентом чемпионата, " +
                    "кодексом этики, а также оборудованием и рабочими местами на конкурсной площадке, протестировать оборудование " +
                    "в течении необходимого для ознакомления времени (не менее 2 часов), получены и изучены инструкции по " +
                    "использованию инструментов, расходынми материалами. Конкурсную документацию внимательно изучил, вопросов не имею," +
                    "умение пользоваться оборудованием и расходными материалами подтверждаю. Инструктаж по Правилам охраны труда получил " +
                    "в полном объеме, обязуюсь соблюдать все требования.";
            }
            if (thisprotocol.id == 5)
            {
                this.Text += " ознакомления с нормативной документацией";
                label_nameprotocol.Text += "об ознакомлении конкурсантов с нормативной документацией";
                dataGridProtocol.Columns["col_comment"].Visible = true;
                label_comment.Text = "Мы, нижеподписавшиеся Конкурсанты, подтверждаем, что нам была предоставлена возможность " +
                    "полноценно ознакомиться с актуальным регламентов чемпионата и кодексом этики.";
            }
            if (thisprotocol.id == 6)
            {
                this.Text += "";
                label_nameprotocol.Text += "об ознакомлении конкурсантов с конкурсной документацией";
                dataGridProtocol.Columns["col_comment"].Visible = true;
            }
            if (thisprotocol.id == 7)
            {
                this.Text += "";
                label_nameprotocol.Text += "Проверка Тулбокса";
                dataGridProtocol.Columns["col_comment"].Visible = true;
            }

            dataGridProtocol.DataSource = null;

            foreach (ClassesForSession4.User user in ClassesForSession4.Dictionaryy.users)
            {
                dataGridProtocol.Rows.Add();
                dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_num"].Value = user.id;
                dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_fio"].Value = user.name;
                dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_dateofbirth"].Value = user.date;
                foreach (ClassesForSession4.ProtocolUser protocol in user.protocol)
                {
                    if (protocol.id == thisprotocol.id)
                    {
                        dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_place"].Value = protocol.place;
                        dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_comment"].Value = protocol.comment;
                        if (protocol.flag)
                        {
                            dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_pin"].Value = "Подписан";
                            dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_pin"].ReadOnly = true;
                            dataGridProtocol.Rows[dataGridProtocol.Rows.Count - 1].Cells["col_pin"].Style.BackColor = Color.GreenYellow;
                        }
                    }
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            thisprotocol.expert = textBox1.Text;
        }

        private void dataGridProtocol_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try { 
            if (e.ColumnIndex == 5)
            {
                foreach (string code in Program.codes)
                {
                    if (code == dataGridProtocol.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString())
                    {
                        dataGridProtocol.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "Подписан";
                        dataGridProtocol.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly = true;
                        dataGridProtocol.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.GreenYellow;

                        foreach (ClassesForSession4.User user in ClassesForSession4.Dictionaryy.users)
                        {
                            if (user.id == Convert.ToInt32(dataGridProtocol.Rows[e.RowIndex].Cells[0].Value))
                            {
                                foreach (ClassesForSession4.ProtocolUser protocol in user.protocol)
                                {
                                    if (protocol.id == thisprotocol.id) protocol.flag = true;
                                }
                            }
                        }
                    }
                }
            }

            if (e.ColumnIndex == 4)
            {
                foreach (string code in Program.codes)
                {
                     foreach (ClassesForSession4.User user in ClassesForSession4.Dictionaryy.users)
                     {
                            if (user.id.ToString() == dataGridProtocol.Rows[e.RowIndex].Cells[0].Value.ToString())
                            {
                                foreach (ClassesForSession4.ProtocolUser protocol in user.protocol)
                                {
                                    if (protocol.id == thisprotocol.id) protocol.comment = dataGridProtocol.Rows[e.RowIndex].Cells[4].Value.ToString();
                                    return;
                                }
                            }
                     }
                }
            }
            }
            catch { }
        }

        private void ProtocolSession4_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormProtocolsSession4 f = new FormProtocolsSession4();
            f.Show();
        }
    }
}
