﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldSkillsAppForms.Session4
{
    public class ClassesForSession4
    {
        public class User
        {
            public int id { set; get; }
            public string name { set; get; }
            public string date { set; get; }
            public List<ProtocolUser> protocol { set; get; }

            public User(int id, string name, string date, List<ProtocolUser> protocol)
            {
                this.id = id;
                this.name = name;
                this.date = date;
                this.protocol = protocol;
            }
        }

        public class Protocol
        {
            public int id { set; get; }
            public string name { set; get; }
            public string ge { set; get; }
            public string expert { set; get; }

            public Protocol(int id, string name, string ge, string expert)
            {
                this.id = id;
                this.name = name;
                this.ge = ge;
                this.expert = expert;
            }
        }

        public class ProtocolUser
        {
            public int id { set; get; }
            public bool flag { set; get; }
            public string place { set; get; }
            public string comment { set; get; }

            public ProtocolUser(int id, bool flag, string place, string comment)
            {
                this.id = id;
                this.flag = flag;
                this.place = place;
                this.comment = comment;
            }
        }

        public static class Dictionaryy
        {
            public static List<Protocol> protocols = new List<Protocol>
        {
            new Protocol(1, "Регистрация и соответсвие возрастному цензу", "Замятин Г.А.", "Греков А.С."),
            new Protocol(2, "Ознакомление участников с техникой безопасности", "Замятин Г.А.", "Андреев А.В."),
            new Protocol(3, "Жеребьевка и распределение конкурсных мест", "Сидорова М.С.", ""),
            new Protocol(4, "Ознакомление с рабочими местами, конкурсной документацией и оборудованием", "Замятин Г.А.", "Федоров Ф.К."),
            new Protocol(5, "Ознакомление с нормативной документацией", "Замятин Г.А.", "Леонов В.Ф."),
            new Protocol(6, "Ознакомление с конкурсной документацией", "Замятин Г.А.", "Прокопов В.М."),
            new Protocol(7, "Проверка Тулбокса", "Замятин Г.А.", "Клопов А.А."),
        };

            public static List<User> users = new List<User> {
            new User(1, "Иванов К.С.", "12.02.2000", new List<ProtocolUser>() {new ProtocolUser(1, true, "2", ""), new ProtocolUser(2, true, "2", ""),
                new ProtocolUser(3, false, "2", "мой комментарий"), new ProtocolUser(4, false, "2", ""), new ProtocolUser(5,true , "2", "мой комментарий"), new ProtocolUser(6,false , "2", ""), new ProtocolUser(7, false, "2", "")}),
            new User(2, "Петров С.М.", "24.02.2000",new List<ProtocolUser>() {new ProtocolUser(1, true, "5", "мой комментарий"), new ProtocolUser(2, true, "5", ""),
                new ProtocolUser(3, true, "5", ""), new ProtocolUser(4, false, "5", ""), new ProtocolUser(5, false, "5", "мой комментарий"), new ProtocolUser(6, true, "5", ""), new ProtocolUser(7, true, "5", "")}),
             new User(3, "Агипов В.А", "10.20.2000", new List<ProtocolUser>() {new ProtocolUser(1, true, "7", ""), new ProtocolUser(2, true, "7", ""),
                new ProtocolUser(3, false, "7", ""), new ProtocolUser(4, false, "7", "мой комментарий"), new ProtocolUser(5, false, "7", "мой комментарий"), new ProtocolUser(6, false, "7", "мой комментарий"), new ProtocolUser(7, true, "8", "")}),
        };
        }
    }
}
