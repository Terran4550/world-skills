﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms.Session4
{
    public partial class AutorizationOnCode : Form
    {
        int colpop = 0;
        public AutorizationOnCode()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colpop > 2)
            {
                this.Enabled = false;
                return;
            }

            foreach (string code in Program.codes)
            {
                if (code == textBox1.Text)
                {
                    MenuSession4 fm2 = new MenuSession4();
                    fm2.Show();
                    return;
                }
            }
            colpop++;
        }
    }
}
