﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorldSkillsAppForms.Session4
{
    public partial class FormProtocolsSession4 : Form
    {

        public FormProtocolsSession4()
        {
            InitializeComponent();
            dataGridView1.DataSource = null;
            foreach (ClassesForSession4.User user in ClassesForSession4.Dictionaryy.users)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = user.id;
                dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[1].Value = user.name;
            }

            dataGridView2.DataSource = null;
            foreach (ClassesForSession4.Protocol prot in ClassesForSession4.Dictionaryy.protocols)
            {
                dataGridView2.Rows.Add();
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[0].Value = prot.id;
                dataGridView2.Rows[dataGridView2.Rows.Count - 1].Cells[1].Value = prot.name;
            }
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                foreach (ClassesForSession4.Protocol protocol in ClassesForSession4.Dictionaryy.protocols)
                {
                    if (protocol.id.ToString() == dataGridView2.SelectedRows[0].Cells[0].Value.ToString())
                    {
                        ProtocolSession4 f = new ProtocolSession4(protocol);
                        f.Show();
                        this.Close();
                        return;
                    }
                }
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView2.ClearSelection();
            if (dataGridView1.SelectedRows.Count > 0)
            {

                foreach (ClassesForSession4.User user in ClassesForSession4.Dictionaryy.users)
                {
                    if (user.id == Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value))
                    {
                        foreach (ClassesForSession4.ProtocolUser protUser in user.protocol)
                        {
                            for (int i = 0; i < dataGridView2.RowCount; i++)
                            {
                                if (Convert.ToInt32(dataGridView2.Rows[i].Cells[0].Value) == protUser.id)
                                {
                                    if (protUser.flag)
                                    {
                                        dataGridView2.Rows[i].Cells[2].Value = "Подписан";
                                        dataGridView2.Rows[i].Cells[2].Style.BackColor = Color.GreenYellow;
                                    }
                                    else
                                    {
                                        dataGridView2.Rows[i].Cells[2].Value = "Не подписан";
                                        dataGridView2.Rows[i].Cells[2].Style.BackColor = Color.Red;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
